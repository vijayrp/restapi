<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('fpsapi');
    }

    public function token_post()
    {
        $data = array('eid' => $this->input->post('eid'));
        $user = $this->fpsapi->getemployee($data);
        if ($user != null) {

            $tokenData = array();
            $tokenData['id'] = $user->eid;
            $output['token'] = AUTHORIZATION::generateToken($tokenData);
            $this->set_response($output, REST_Controller::HTTP_OK);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_UNAUTHORIZED,
            'message' => 'Unauthorized',
        ];
        
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }

}