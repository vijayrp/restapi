<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
    
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
	 * http://www.2my4edge.com/2016/04/multiple-image-upload-with-view-edit.html
     */
    
    
    function __construct()
    {
   
	header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: GET, POST, PUT');
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('fpsapi');
    }
    
	
	
	 public function messagelist()
    {
        header('Content-Type: application/json');
        $message = $this->fpsapi->get_messagelist();
        if ($message) {
            echo json_encode($message);
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
    }
	
		
	 public function get_messagebyid($mid,$dlete)
    {
        header('Content-Type: application/json');
		$data_value = array(
		'id' =>$dlete,
		'action'    => $mid);
		
        $message = $this->fpsapi->get_messagebyid($data_value);
        if ($message) {
            echo json_encode($message[0]);
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
    }
	
	
    
    public function messageoftheday()
    {
        header('Content-Type: application/json');
        $message = $this->fpsapi->get_message();
        if ($message) {
            echo json_encode($message);
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
    }
	
	  public function add_messageoftheday()
    {
        header('Content-Type: application/json');
		
              $files = $_FILES;
              
                $_FILES['fileToUpload']['name']= time().$files['fileToUpload']['name'];
                $_FILES['fileToUpload']['type']= $files['fileToUpload']['type'];
                $_FILES['fileToUpload']['tmp_name']= $files['fileToUpload']['tmp_name'];
                $_FILES['fileToUpload']['error']= $files['fileToUpload']['error'];
                $_FILES['fileToUpload']['size']= $files['fileToUpload']['size'];
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('fileToUpload');
                $fileName = $_FILES['fileToUpload']['name'];
                $images = $fileName;
				

       	
		$data = array(
		'title' => $this->input->post('title'),
		'message' => $this->input->post('message'),
		'image' => $images,
		'date' => $this->input->post('date'),
		'enddate' => $this->input->post('enddate')
		);
		
        $add_message = $this->fpsapi->add_messageoftheday($data);
        
       if ($add_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message added successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not insert...';
            print_r(json_encode($data));
        } 

		 redirect('https://dashboard-stg.fpsinc.com/#/messagemanager'); 
    }
	
	 public function update_messageoftheday()
    {
        header('Content-Type: application/json');
		
		if($_FILES['fileToUpload']['name']){
		       $files = $_FILES;
                $_FILES['fileToUpload']['name']= time().$files['fileToUpload']['name'];
                $_FILES['fileToUpload']['type']= $files['fileToUpload']['type'];
                $_FILES['fileToUpload']['tmp_name']= $files['fileToUpload']['tmp_name'];
                $_FILES['fileToUpload']['error']= $files['fileToUpload']['error'];
                $_FILES['fileToUpload']['size']= $files['fileToUpload']['size'];
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('fileToUpload');
                $fileName = $_FILES['fileToUpload']['name'];
                $images = $fileName;
				$poat_data['image'] = $images;
		        
		}
		
		$poat_data = array(
		'id'    => $this->input->post('eid'),
		'title' => $this->input->post('title'),
		'message' => $this->input->post('message'),
		'date' => $this->input->post('date'),
		'enddate' => $this->input->post('enddate')
		);
		
        $update_message = $this->fpsapi->update_messageoftheday($poat_data);
        
       if ($update_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message updated successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not updated...';
            print_r(json_encode($data));
        }         
        redirect('https://dashboard-stg.fpsinc.com/#/messagemanager'); 
    }
	
	public function deletemessage($mid,$id){
		
		$data = array(
		'mid'    => $id);
		
	  $delete_message = $this->fpsapi->delete_messageoftheday($data);	
		
		if ($delete_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message delete successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not delete...';
            print_r(json_encode($data));
        }      
	}
	
    public function agents()
    {
        header('Content-Type: application/json');
        $agents = $this->fpsapi->getagents();
        
        if ($agents) {
            
            echo json_encode($agents);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
      
    public function escalation()
    {
        header('Content-Type: application/json');
        $escalation = $this->fpsapi->get_escalation();
        
        if ($escalation) {
            
            echo json_encode($escalation);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
     public function dispolist()
    {
        header('Content-Type: application/json');
        $Dispolist = $this->fpsapi->getDispolist();
        
        if ($Dispolist) {
            
            echo json_encode($Dispolist);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	 public function get_PostCallRetentionList($action,$eid)
    {
        header('Content-Type: application/json');
		
		$data = array('assigned_eid' => $eid,'action'=>'get');
		
        $Dispolist = $this->fpsapi->get_post_call_retention_list($data);
        
        if ($Dispolist) {
            
            echo json_encode($Dispolist);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
    }
	
	
	
	   public function get_PostCallRetention($call_datetime,$eid,$ban)
    {
        header('Content-Type: application/json');
		
		$data = array(
		'call_datetime' => urldecode($call_datetime),
		'eid' => $eid,
		'ban' => $ban
		);
		
        $Dispolist = $this->fpsapi->get_post_call_retention($data);
        
        if ($Dispolist) {
            
            echo json_encode($Dispolist[0]);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	 public function update_PostCallRetention()
    {
        header('Content-Type: application/json');
	$data_json = json_decode(file_get_contents("php://input"));
		$data_value = array(
		'call_datetime' => $data_json->call_datetime,
		'eid' => $data_json->eid,
		'employee_name' => $data_json->employee_name,
		'ban' => $data_json->ban,
		'dispo_id' => $data_json->dispo_id,
		'called_yn' => $data_json->called_yn,
		'contacted_yn' => $data_json->contacted_yn,
		'voicemail_yn' => $data_json->voicemail_yn,
		'opportunity_yn' => $data_json->opportunity_yn,
		'save_yn' => $data_json->save_yn,
		'rpo_upg_both' => $data_json->rpo_upg_both,
		'at_risk_yn' => $data_json->at_risk_yn,
		'notes' => $data_json->notes);
		
        $PostCallRetention = $this->fpsapi->update_PostCallRetention($data_value);
        
        if ($PostCallRetention) {
            
            $data['status']  = 'success';
            $data['message'] = 'Data updated successfully...';
            print_r(json_encode($data));
            
        } else {
			
            $data['status']  = $data_value;
            $data['message'] = 'Sorry data not updated...';
            print_r(json_encode($data));
        }      
        
    }
	 
	
	 public function managers()
    {
        header('Content-Type: application/json');
        $managers = $this->fpsapi->get_managers();
        
        if ($managers) {
            
            echo json_encode($managers);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        } 
        
    }
	
	 public function get_agent($eid,$role_id)
    {
        header('Content-Type: application/json');
		
		$data = array(
		'eid' => $eid,	
		'role_id' => $role_id
		);
		
        $get_agent = $this->fpsapi->get_agent($data);
        
        if ($get_agent) {
            
            echo json_encode($get_agent);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
    }
	
	
	 public function ManagerStats($month,$eid)
    {
        header('Content-Type: application/json');
		
		$data = array(
		'eid' => $eid,
		'month' => $month);
		
        $ManagerStats = $this->fpsapi->get_ManagerStats($data);
        
        if ($ManagerStats) {
            
            echo json_encode($ManagerStats);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	 public function SupervisorStats($month)
    {
        header('Content-Type: application/json');
		
		$data = array('month' => $month);
		
        $SupervisorStats = $this->fpsapi->SupervisorStats($month);
        
        if ($SupervisorStats) {
            
            echo json_encode($SupervisorStats);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	
	 public function get_areachrat($eid,$day_month,$chart_data)
    {
        header('Content-Type: application/json');
		
		$array = array(); 
		$data = array(
		'eid' => $eid,
		'day_month' => $day_month,
		'chart_data' => $chart_data
		);
        $areachrat = $this->fpsapi->get_areachart($data);
        
        if ($areachrat) {
			
		$array['cols'][] = array('type' => 'string'); 
        $array['cols'][] = array('type' => 'number');
        $i=0;
        foreach($areachrat as $chart){
			if($data['chart_data']=='avg_daily_calls'){
        $array['rows'][$i]['c'] = array(array('v' => $chart->date),array('v' => round($chart->$data['chart_data'])));
			}else{
		 $array['rows'][$i]['c'] = array(array('v' => $chart->date),array('v' => round($chart->$data['chart_data']*100)));		
			}
        $i++;
		}		     
            echo json_encode($array);          
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	public function get_gauges($GaugesData,$eid){
		
		header('Content-Type: application/json');
		$data=array();
		
		$data['eid'] = $eid;
		$data['end_date']='';
		$data['start_date']='';
		
			
		if($GaugesData=='PreviousBusinessDay'){ /// ***  Gauges Data for Previous Business Day  *** ////
			$getday = date('l', strtotime(date('Y-m-d')));
			$day_of_week = date('N', strtotime($getday));
			if($day_of_week==1) {	
			$data['start_date'] = date('Y-m-d', strtotime('-3 days')); 
			$data['end_date'] = date('Y-m-d'); 	
			}elseif($day_of_week==6) {	
			$data['start_date'] = date('Y-m-d', strtotime('-1 days')); 
			$data['end_date'] = date('Y-m-d'); 
			}elseif($day_of_week==7) {
			$data['start_date'] = date('Y-m-d', strtotime('-2 days')); 
			$data['end_date'] = date('Y-m-d'); 
			}else {
			$data['start_date'] = date('Y-m-d', strtotime('-1 days')); 
			$data['end_date'] = date('Y-m-d');
			}	  
			$data['end_date'] = date('Y-m-d');		
			
			
		}elseif($GaugesData=='CurrentMonth'){ /// ***  Gauges Data for Current Month  *** ////
			$data['end_date'] = date('Y-m-d', strtotime('-1 days')); 
            $data['start_date'] = date('Y-m').'-01';	
			
		}
		
		$data_value = array(
		'eid' => $eid,
		'start_date' => $data['start_date'],
		'end_date' => $data['end_date']
		);
		
        $gauges = $this->fpsapi->get_gauges($data_value);
        
        if ($gauges) {
            
            echo json_encode($gauges);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
	}
	
	public function get_agent_scorecard_sprint($eid,$month)
    {
        header('Content-Type: application/json');
		
		$data_value = array(
		'eid' => $eid,
		'month' => $month);
		
        $agent_scorecard = $this->fpsapi->get_agent_scorecard_sprint($data_value);
        
        if ($agent_scorecard) {
            
            echo json_encode($agent_scorecard[0]);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	public function get_agent_scorecard_spark_sprint($value_data,$eid)
    {
        header('Content-Type: application/json');
		
		$data_value = array(
		'value_data' => $value_data,
		'eid' => $eid);
		
        $agent_scorecard = $this->fpsapi->get_agent_scorecard_spark_sprint($data_value);
        
       if ($agent_scorecard) {
		   
		   
		$array['cols'][] = array('type' => 'string'); 
        $array['cols'][] = array('type' => 'number');
        $i=0;
	
		
        foreach($agent_scorecard as $chart){
			switch($value_data){
			
            case 'calls_per_day_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->calls_per_day_actual,1)));	
			 break;
			 case 'attendance_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->attendance_actual*100,1)));	
			 break;
			 case 'nba_launch_rate_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->nba_launch_rate_actual*100,1)));	
			 break;
			 case 'deact_port_rate_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->deact_port_rate_actual*100,1)));	
			 break;
			 case 'pic14_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->pic14_actual*100,1)));	
			 break;
			 case 'pic30_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->pic30_actual*100,1)));	
			 break;
			}	
        $i++;
		}

			 
            echo json_encode($array);  
			
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
      
    
    
}