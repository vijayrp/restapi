<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
    
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
	 * http://www.2my4edge.com/2016/04/multiple-image-upload-with-view-edit.html
     */
    
    
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('fpsapi');
    }
    
    
    public function messageoftheday()
    {
        header('Content-Type: application/json');
        $message = $this->fpsapi->get_message();
        if ($message) {
            echo json_encode($message);
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
    }
	
	  public function add_messageoftheday()
    {
        header('Content-Type: application/json');
		
              $files = $_FILES;
              $count = count($_FILES['fileToUpload']['name']);
              for($i=0; $i<$count; $i++)
                {
                $_FILES['fileToUpload']['name']= time().$files['fileToUpload']['name'][$i];
                $_FILES['fileToUpload']['type']= $files['fileToUpload']['type'][$i];
                $_FILES['fileToUpload']['tmp_name']= $files['fileToUpload']['tmp_name'][$i];
                $_FILES['fileToUpload']['error']= $files['fileToUpload']['error'][$i];
                $_FILES['fileToUpload']['size']= $files['fileToUpload']['size'][$i];
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
                $fileName = $_FILES['fileToUpload']['name'];
                $images[] = $fileName;
                }	
				
        $image_name = implode(',',$images);
				
		$data = array(
		'title' => $this->input->post('title'),
		'message' => $this->input->post('message'),
		'image' => $image_name,
		'date' => $this->input->post('date'),
		'enddate' => $this->input->post('enddate')
		);
		
        $add_message = $this->fpsapi->add_messageoftheday($data);
        
       if ($add_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message added successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not insert...';
            print_r(json_encode($data));
        }        
    }
	
	 public function update_messageoftheday()
    {
        header('Content-Type: application/json');
		
		$poat_data = array(
		'id'    => $this->input->post('mid'),
		'title' => $this->input->post('title'),
		'message' => $this->input->post('message'),
		'image' => $this->input->post('image'),
		'date' => $this->input->post('date'),
		'enddate' => $this->input->post('enddate')
		);
		
        $update_message = $this->fpsapi->update_messageoftheday($poat_data);
        
       if ($update_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message updated successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not updated...';
            print_r(json_encode($data));
        }         
        
    }
	
	public function deletemessage($mid,$id){
		
		$data = array(
		'mid'    => $id);
		
	  $delete_message = $this->fpsapi->delete_messageoftheday($data);	
		
		if ($delete_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message delete successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not delete...';
            print_r(json_encode($data));
        }      
	}
	
    public function agents()
    {
        header('Content-Type: application/json');
        $agents = $this->fpsapi->getagents();
        
        if ($agents) {
            
            echo json_encode($agents);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
      
    public function escalation()
    {
        header('Content-Type: application/json');
        $escalation = $this->fpsapi->get_escalation();
        
        if ($escalation) {
            
            echo json_encode($escalation);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
     public function dispolist()
    {
        header('Content-Type: application/json');
        $Dispolist = $this->fpsapi->getDispolist();
        
        if ($Dispolist) {
            
            echo json_encode($Dispolist);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	   public function get_PostCallRetention($call_datetime,$eid,$ban)
    {
        header('Content-Type: application/json');
		
		$data = array(
		'call_datetime' => date('Y-m-d H:i:s', strtotime($call_datetime)),
		'eid' => $eid,
		'ban' => $ban
		);
		
        $Dispolist = $this->fpsapi->get_post_call_retention($data);
        
        if ($Dispolist) {
            
            echo json_encode($Dispolist);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	 public function update_PostCallRetention()
    {
        header('Content-Type: application/json');
	
		$poat_data = array(
		'call_datetime' => $this->input->post('call_datetime'),
		'eid' => $this->input->post('eid'),
		'employee_name' => $this->input->post('employee_name'),
		'ban' => $this->input->post('ban'),
		'dispo_id' => $this->input->post('dispo_id'),
		'called_yn' => $this->input->post('called_yn'),
		'contacted_yn' => $this->input->post('contacted_yn'),
		'voicemail_yn' => $this->input->post('voicemail_yn'),
		'opportunity_yn' => $this->input->post('opportunity_yn'),
		'save_yn' => $this->input->post('save_yn'),
		'rpo_upg_both' => $this->input->post('rpo_upg_both'),
		'at_risk_yn' => $this->input->post('at_risk_yn'),
		'notes' => $this->input->post('notes'));
		
        $PostCallRetention = $this->fpsapi->update_post_call_retention($poat_data);
        
        if ($PostCallRetention) {
            
            $data['status']  = 'success';
            $data['message'] = 'Data updated successfully...';
            print_r(json_encode($data));
            
        } else {
			
            $data['status']  = 'failed';
            $data['message'] = 'Sorry data updated...';
            print_r(json_encode($data));
        }      
        
    }
	 
	
	 public function managers()
    {
        header('Content-Type: application/json');
        $managers = $this->fpsapi->get_managers();
        
        if ($managers) {
            
            echo json_encode($managers);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	 public function ManagerStats($eid,$month,$role_id)
    {
        header('Content-Type: application/json');
		
		$data = array(
		'eid' => $eid,
		'month' => $month,
		'role_id' => $role_id
		);
		
        $ManagerStats = $this->fpsapi->get_ManagerStats($data);
        
        if ($ManagerStats) {
            
            echo json_encode($ManagerStats);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	 public function get_areachrat($eid,$day_month,$chart_data)
    {
        header('Content-Type: application/json');
		
		$array = array(); 
		$data = array(
		'eid' => $eid,
		'day_month' => $day_month,
		'chart_data' => $chart_data
		);
        $areachrat = $this->fpsapi->get_areachart($data);
        
        if ($areachrat) {
			
		$array['cols'][] = array('type' => 'string'); 
        $array['cols'][] = array('type' => 'number');
        $i=0;
        foreach($areachrat as $chart){
			if($data['chart_data']=='avg_daily_calls'){
        $array['rows'][$i]['c'] = array(array('v' => $chart->date),array('v' => round($chart->$data['chart_data'])));
			}else{
		 $array['rows'][$i]['c'] = array(array('v' => $chart->date),array('v' => round($chart->$data['chart_data']*100)));		
			}
        $i++;
		}		     
            echo json_encode($array);          
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	public function get_gauges($GaugesData,$eid){
		
		header('Content-Type: application/json');
		$data=array();
		
		$data['eid'] = $eid;
		$data['end_date']='';
		$data['start_date']='';
		
			
		if($GaugesData=='PreviousBusinessDay'){ /// ***  Gauges Data for Previous Business Day  *** ////
			$getday = date('l', strtotime(date('Y-m-d')));
			$day_of_week = date('N', strtotime($getday));
			if($day_of_week==1) {	
			$data['start_date'] = date('Y-m-d', strtotime('-3 days')); 
			$data['end_date'] = date('Y-m-d'); 	
			}elseif($day_of_week==6) {	
			$data['start_date'] = date('Y-m-d', strtotime('-1 days')); 
			$data['end_date'] = date('Y-m-d'); 
			}elseif($day_of_week==7) {
			$data['start_date'] = date('Y-m-d', strtotime('-2 days')); 
			$data['end_date'] = date('Y-m-d'); 
			}else {
			$data['start_date'] = date('Y-m-d', strtotime('-1 days')); 
			$data['end_date'] = date('Y-m-d');
			}	  
			$data['end_date'] = date('Y-m-d');		
			
			
		}elseif($GaugesData=='CurrentMonth'){ /// ***  Gauges Data for Current Month  *** ////
			$data['end_date'] = date('Y-m-d', strtotime('-1 days')); 
            $data['start_date'] = date('Y-m').'-01';	
			
		}
		
		$data_value = array(
		'eid' => $eid,
		'start_date' => $data['start_date'],
		'end_date' => $data['end_date']
		);
		
        $gauges = $this->fpsapi->get_gauges($data_value);
        
        if ($gauges) {
            
            echo json_encode($gauges);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
	}
	
	public function get_agent_scorecard_sprint($eid,$month)
    {
        header('Content-Type: application/json');
		
		$data = array(
		'eid' => $eid,
		'month' => $month
		);
		
        $agent_scorecard = $this->fpsapi->get_agent_scorecard_sprint($data);
        
        if ($agent_scorecard) {
            
            echo json_encode($agent_scorecard);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
      
    
    
}