<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class V2 extends REST_Controller
{
    
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
	 * http://www.2my4edge.com/2016/04/multiple-image-upload-with-view-edit.html
     */
    
    
    function __construct()
    {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
		header('Access-Control-Allow-Methods: GET, POST, PUT');
		
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('fpsapi');

        //require_once 'HTTP/Request2.php';
    }
    
	private function is_authenticate()
    {
        try {
            $headers = $this->input->request_headers();
            if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

                $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
                if ($decodedToken != false) {

                    $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                    return true;
                }
            }
            $this->set_response("Unauthorised", REST_Controller::HTTP_UNAUTHORIZED);

        } catch (Exception $e) {
            //echo 'Error: ' . $e->getMessage();
            return false;
        }

    }
	
	 public function message_get()
    {
        header('Content-Type: application/json');
        if($this->is_authenticate()) {

            if ($this->get('mid')) {
                $data_value = array(
                'id' =>$this->get('mid'),
                'action'    => $this->get('mid'));
                $message = $this->fpsapi->get_messagebyid($data_value);
            } else {
                $message = $this->fpsapi->get_messagelist();
            }
            if ($message) {
                echo json_encode($message);
            } else {
                $data['status']  = 'failed';
                $data['message'] = 'No data found...';
                print_r(json_encode($data));
            }
        } else {
            //$data['status']  = REST_Controller::HTTP_UNAUTHORIZED;
            $data['status']  = 'failed';
            $data['message'] = 'Unauthorised access...';
            print_r(json_encode($data));
        }
    }
    
/*    public function messageoftheday()
    {
        header('Content-Type: application/json');
        $message = $this->fpsapi->get_message();
        if ($message) {
            echo json_encode($message);
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
    }*/
	
	  public function message_post()
    {
        header('Content-Type: application/json');
		
              $files = $_FILES;
              
                $_FILES['fileToUpload']['name']= time().$files['fileToUpload']['name'];
                $_FILES['fileToUpload']['type']= $files['fileToUpload']['type'];
                $_FILES['fileToUpload']['tmp_name']= $files['fileToUpload']['tmp_name'];
                $_FILES['fileToUpload']['error']= $files['fileToUpload']['error'];
                $_FILES['fileToUpload']['size']= $files['fileToUpload']['size'];
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('fileToUpload');
                $fileName = $_FILES['fileToUpload']['name'];
                $images = $fileName;
				

       	
		$data = array(
		'title' => $this->input->post('title'),
		'message' => $this->input->post('message'),
		'image' => $images,
		'date' => $this->input->post('date'),
		'enddate' => $this->input->post('enddate')
		);
		
        $add_message = $this->fpsapi->add_messageoftheday($data);
        
       if ($add_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message added successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not insert...';
            print_r(json_encode($data));
        } 

		 redirect('https://dashboard-stg.fpsinc.com/#/messagemanager'); 
    }
	
	 public function message_put()
    {
        header('Content-Type: application/json');
		
	
		if($_FILES['fileToUpload']['name']){
		       $files = $_FILES;
                $_FILES['fileToUpload']['name']= time().$files['fileToUpload']['name'];
                $_FILES['fileToUpload']['type']= $files['fileToUpload']['type'];
                $_FILES['fileToUpload']['tmp_name']= $files['fileToUpload']['tmp_name'];
                $_FILES['fileToUpload']['error']= $files['fileToUpload']['error'];
                $_FILES['fileToUpload']['size']= $files['fileToUpload']['size'];
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('fileToUpload');
                $fileName = $_FILES['fileToUpload']['name'];
                $images = $fileName;
				$poat_data['image'] = $images;
		        
		}
		
		
		 
		
		
		$poat_data = array(
		'id'    => $this->put('eid'),
		'title' => $this->put('title'),
		'message' => $this->put('message'),
		'date' => $this->put('date'),
		'enddate' => $this->put('enddate')
		);
		
        $update_message = $this->fpsapi->update_messageoftheday($poat_data);
        
       if ($update_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message updated successfully...';
            print_r(json_encode(poat_data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not updated...';
            print_r(json_encode($poat_data));
        }         
       //redirect('https://dashboard-stg.fpsinc.com/#/messagemanager'); 
    }
	
	public function message_delete($mid,$id){
		
		$data = array(
		'mid'    => $id);
		
	  $delete_message = $this->fpsapi->delete_messageoftheday($data);	
		
		if ($delete_message) {
            $data['status']  = 'success';
            $data['message'] = 'Message delete successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry message not delete...';
            print_r(json_encode($data));
        }      
	}
	
    public function agent_get()
    {
        header('Content-Type: application/json');
		

        if ($this->get('id')) {
            $data = array(
            'id' => $this->get('id'));
            $agents = $this->fpsapi->get_agent($data);
        } else {
            $agents = $this->fpsapi->getagents();
        }
        if ($agents) {
            
            echo json_encode($agents);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
    }
	
	
	 public function manager_get()
    {
        header('Content-Type: application/json');
		
		if ($this->get('id')) {
            $data = array(
            'id' => $this->get('id'));
         $managers = $this->fpsapi->get_manager($data);
		  if ($managers) {
            
            echo json_encode($managers[0]);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        } 
		 
        } else {
            $managers = $this->fpsapi->managerslist();
		   
		    if ($managers) {
            
            echo json_encode($managers);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        } 
        }
		
		 
       
        
    }
	
	
      
    public function escalation_get()
    {
        header('Content-Type: application/json');
        $escalation = $this->fpsapi->get_escalation();
        
        if ($escalation) {
            
            echo json_encode($escalation);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
    }
	
    public function escalation_post()
    {
        header('Content-Type: application/json');
        $data_json = json_decode(file_get_contents("php://input"));
        
        $data_value = array(
        'calldatetime' => $data_json->calldatetime,
        'agentname' => $data_json->agentname,
        'customername' => $data_json->customername,
        'bannumber' => $data_json->bannumber,
        'marketvalue' => $data_json->marketvalue,
        'nolines' => $data_json->nolines,
        'nbaoffers' => $data_json->nbaoffers,
        'nbareason' => $data_json->nbareason,
        'description' => $data_json->description,
        'stepstaken' => $data_json->stepstaken,
        'customerdesire' => $data_json->customerdesire,
        'manager' => $data_json->manager,
        'callbackdatetime' => $data_json->callbackdatetime,
        'status' => $data_json->status);
        
        
        // $data_value = array(
        // 'calldatetime' => $this->input->post('calldatetime'),
        // 'agentname' => $this->input->post('agentname'),
        // 'customername' => $this->input->post('customername'),
        // 'bannumber' => $this->input->post('bannumber'),
        // 'marketvalue' => $this->input->post('marketvalue'),
        // 'nolines' => $this->input->post('nolines'),
        // 'nbaoffers' => $this->input->post('nbaoffers'),
        // 'nbareason' => $this->input->post('nbareason'),
        // 'description' => $this->input->post('description'),
        // 'stepstaken' => $this->input->post('stepstaken'),
        // 'customerdesire' => $this->input->post('customerdesire'),
        // 'manager' => $this->input->post('manager'),
        // 'callbackdatetime' => $this->input->post('callbackdatetime'),
        // 'status' => $this->input->post('status'));
        
        
        $PostCallRetention = $this->fpsapi->postescalation($data_value);
        
        if ($PostCallRetention) {
            
            $data['status']  = 'success';
            $data['message'] = 'Data updated successfully...';
            print_r(json_encode($data));
            
        } else {
            
            $data['status']  = 'Error';
            $data['message'] = 'Sorry data not updated...';
            print_r(json_encode($data));
        }      
        
            
    }
	
     public function post_call_retention_dispo_get()
    {
        header('Content-Type: application/json');
        $Dispolist = $this->fpsapi->getDispolist();
        
        if ($Dispolist) {
            
            echo json_encode($Dispolist);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
    }
	
	
	 public function post_call_retention_get()
    {
        header('Content-Type: application/json');
		
        if ($this->get('call_datetime')) {
            $data = array(
                'call_datetime' => urldecode($this->get($call_datetime)),
                'eid' => $this->get('eid'),
                'ban' => $this->get('ban')
                );
            $dispoList = $this->fpsapi->get_post_call_retention($data);
        } else if ($this->get('eid')){

            $eid = $this->get('eid');
            $data = array('assigned_eid' => $eid);
            $dispoList = $this->fpsapi->get_post_call_retention_list($data);
        }
        if ($dispoList) {
            
            echo json_encode($dispoList);

        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
    }
	
	
	 public function post_call_retention_put()
    {
        header('Content-Type: application/json');
	  $data_json = json_decode(file_get_contents("php://input"));
		$data_value = array(
		'call_datetime' => $data_json->call_datetime,
		'eid' => $data_json->eid,
		'employee_name' => $data_json->employee_name,
		'ban' => $data_json->ban,
		'dispo_id' => $data_json->dispo_id,
		'called_yn' => $data_json->called_yn,
		'contacted_yn' => $data_json->contacted_yn,
		'voicemail_yn' => $data_json->voicemail_yn,
		'opportunity_yn' => $data_json->opportunity_yn,
		'save_yn' => $data_json->save_yn,
		'rpo_upg_both' => $data_json->rpo_upg_both,
		'at_risk_yn' => $data_json->at_risk_yn,
		'notes' => $data_json->notes);
		
        $postCallRetention = $this->fpsapi->update_PostCallRetention($data_value);
        
        if ($postCallRetention) {
            
            $data['status']  = 'success';
            $data['message'] = 'Data updated successfully...';
            print_r(json_encode($data));
            
        } else {
			
            $data['status']  = $data_value;
            $data['message'] = 'Sorry data not updated...';
            print_r(json_encode($data));
        }      
        
    }
	 
	
	
	

	 public function manager_status_get()
    {
        header('Content-Type: application/json');
		
        $eid = $this->get('eid');
        $month = $this->get('month');
        $txtmonth = $this->get('txtmonth');

		$data = array('eid'=>$eid,'month' => $month,'txtmonth'=>$txtmonth);
		
        $ManagerStats = $this->fpsapi->get_ManagerStats($data);
        
        if ($ManagerStats) {
            
            echo json_encode($ManagerStats);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	 public function supervisor_status_get()
    {
        header('Content-Type: application/json');
		
        $eid = $this->get('eid');
        $month = $this->get('month');
        $txtmonth = $this->get('txtmonth');

		$data = array('eid'=>$eid,'month' => $month,'txtmonth'=>$txtmonth);
		
        $SupervisorStats = $this->fpsapi->SupervisorStats($data);
        
        if ($SupervisorStats) {
            
            echo json_encode($SupervisorStats, JSON_PRETTY_PRINT);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        } 
    }
	
	
	
	 public function area_chart_get()
    {
        header('Content-Type: application/json');
		
        $eid = $this->get('eid');
        $day_month = $this->get('day_month');
        $chart_data = $this->get('chart_data');

		$array = array(); 
		$data = array(
		'eid' => $eid,
		'day_month' => $day_month,
		'chart_data' => $chart_data
		);
        $areachrat = $this->fpsapi->get_areachart($data);
        
        if ($areachrat) {
			
		$array['cols'][] = array('type' => 'string'); 
        $array['cols'][] = array('type' => 'number');
        $i=0;
        foreach($areachrat as $chart){
			if($data['chart_data']=='avg_daily_calls'){
        $array['rows'][$i]['c'] = array(array('v' => $chart->date),array('v' => round($chart->$data['chart_data'])));
			}else{
		 $array['rows'][$i]['c'] = array(array('v' => $chart->date),array('v' => round($chart->$data['chart_data']*100)));		
			}
        $i++;
		}		     
            echo json_encode($array);          
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        } 
    }
	
	public function gauges_get(){
		
		header('Content-Type: application/json');
		$data=array();

        $eid = $this->get('eid');
		$GaugesData = $this->get('GaugesData');
		$data['eid'] = $eid;
		$data['end_date']='';
		$data['start_date']='';
		
			
		if($GaugesData=='PreviousBusinessDay'){ /// ***  Gauges Data for Previous Business Day  *** ////
			$getday = date('l', strtotime(date('Y-m-d')));
			$day_of_week = date('N', strtotime($getday));
			if($day_of_week==1) {	
			$data['start_date'] = date('Y-m-d', strtotime('-3 days')); 
			$data['end_date'] = date('Y-m-d'); 	
			}elseif($day_of_week==6) {	
			$data['start_date'] = date('Y-m-d', strtotime('-1 days')); 
			$data['end_date'] = date('Y-m-d'); 
			}elseif($day_of_week==7) {
			$data['start_date'] = date('Y-m-d', strtotime('-2 days')); 
			$data['end_date'] = date('Y-m-d'); 
			}else {
			$data['start_date'] = date('Y-m-d', strtotime('-1 days')); 
			$data['end_date'] = date('Y-m-d');
			}	  
			$data['end_date'] = date('Y-m-d');		
			
			
		}elseif($GaugesData=='CurrentMonth'){ /// ***  Gauges Data for Current Month  *** ////
			$data['end_date'] = date('Y-m-d', strtotime('-1 days')); 
            $data['start_date'] = date('Y-m').'-01';	
			
		}
		
		$data_value = array(
		'eid' => $eid,
		'start_date' => $data['start_date'],
		'end_date' => $data['end_date']
		);
		
        $gauges = $this->fpsapi->get_gauges($data_value);
        
        if ($gauges) {
            
            echo json_encode($gauges);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
	}
	
	public function agent_scorecard_sprint_get()
    {
        header('Content-Type: application/json');
		$eid = $this->get('eid');
        $month = $this->get('month');

		$data_value = array(
		'eid' => $eid,
		'month' => $month);
		
        $agent_scorecard = $this->fpsapi->get_agent_scorecard_sprint($data_value);
        
        if ($agent_scorecard) {
            
            echo json_encode($agent_scorecard[0]);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	public function agent_scorecard_spark_sprint_get()
    {
        header('Content-Type: application/json');
		
        $eid = $this->get('eid');
        $value_data = $this->get('value_data');

		$data_value = array(
		'value_data' => $value_data,
		'eid' => $eid);
		
        $agent_scorecard = $this->fpsapi->get_agent_scorecard_spark_sprint($data_value);
        
       if ($agent_scorecard) {
		   
		   
		$array['cols'][] = array('type' => 'string'); 
        $array['cols'][] = array('type' => 'number');
        $i=0;
	
		
        foreach($agent_scorecard as $chart){
			switch($value_data){
			
            case 'calls_per_day_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->calls_per_day_actual,1)));	
			 break;
			 case 'attendance_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->attendance_actual*100,1)));	
			 break;
			 case 'nba_launch_rate_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->nba_launch_rate_actual*100,1)));	
			 break;
			 case 'deact_port_rate_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->deact_port_rate_actual*100,1)));	
			 break;
			 case 'pic14_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->pic14_actual*100,1)));	
			 break;
			 case 'pic30_ptg':			
			 $array['rows'][$i]['c'] = array(array('v' => $chart->month),array('v' => round($chart->pic30_actual*100,1)));	
			 break;
			}	
        $i++;
		}

			 
            echo json_encode($array);  
			
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	public function metric_tiers_sprint_get()
    {
        header('Content-Type: application/json');
		
        $eid = $this->get('eid');
		$scorecard_lvl = $this->get('scorecard_lvl');

		$data_value = array(
		'eid' => $eid,
		'scorecard_lvl' => $scorecard_lvl);
		
        $agent_scorecard = $this->fpsapi->get_metric_tiers_sprint($data_value);
        
        if ($agent_scorecard) {
			
		 foreach($agent_scorecard as $chart){
			 
			 if($chart->metric=='attendance_actual'){
				 
				$array_scorecard['attendance_actual'][] = array('start_range' => $chart->start_range,'end_range'=>$chart->end_range,'result'=>$chart->result,'range_color'=>$chart->range_color,'result_format'=>$chart->result_format,'range_format'=>$chart->range_format);
			 }
			  if($chart->metric=='calls_per_day_actual'){
				 
				$array_scorecard['calls_per_day_actual'][] = array('start_range' => $chart->start_range,'end_range'=>$chart->end_range,'result'=>$chart->result,'range_color'=>$chart->range_color,'result_format'=>$chart->result_format,'range_format'=>$chart->range_format);
			 }
			 
			 if($chart->metric=='deact_port_rate_actual'){
				 
				$array_scorecard['deact_port_rate_actual'][] = array('start_range' => $chart->start_range,'end_range'=>$chart->end_range,'result'=>$chart->result,'range_color'=>$chart->range_color,'result_format'=>$chart->result_format,'range_format'=>$chart->range_format);
			 }
			 if($chart->metric=='nba_launch_rate_actual'){
				 
				$array_scorecard['nba_launch_rate_actual'][] = array('start_range' => $chart->start_range,'end_range'=>$chart->end_range,'result'=>$chart->result,'range_color'=>$chart->range_color,'result_format'=>$chart->result_format,'range_format'=>$chart->range_format);
			 }
			 if($chart->metric=='pic14_actual'){
				 
				$array_scorecard['pic14_actual'][] = array('start_range' => $chart->start_range,'end_range'=>$chart->end_range,'result'=>$chart->result,'range_color'=>$chart->range_color,'result_format'=>$chart->result_format,'range_format'=>$chart->range_format);
			 }
			 if($chart->metric=='pic30_actual'){
				 
				$array_scorecard['pic30_actual'][] = array('start_range' => $chart->start_range,'end_range'=>$chart->end_range,'result'=>$chart->result,'range_color'=>$chart->range_color,'result_format'=>$chart->result_format,'range_format'=>$chart->range_format);
			 }
			 
		 }
			
			echo json_encode($array_scorecard); 
            
            
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    }
	
	
	
	public function get_agent_scorecard_sprint_new()
    {
        header('Content-Type: application/json');
		
        $eid = $this->get('eid');
        $month = $this->get('month');

		$data_value = array(
		'eid' => $eid,
		'month' => $month);
		
        $agent_scorecard = $this->fpsapi->get_agent_scorecard_sprint_new($data_value);
        
        if ($agent_scorecard) {
            
            echo json_encode($agent_scorecard[0],JSON_PRETTY_PRINT);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        }
        
        
    } 
	
	
	 public function user_preferences_get()
    {
        header('Content-Type: application/json');
		
		if ($this->get('id')) {
            $data = array(
            'id' => $this->get('id'));
         $preferences = $this->fpsapi->get_preferences($data);
		  if ($preferences) {
            
            echo json_encode($preferences[0]);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        } 
		 
        } else {
            $preferences = $this->fpsapi->preferences_list();
		   
		    if ($preferences) {
            
            echo json_encode($preferences);
            
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'No data found...';
            print_r(json_encode($data));
        } 
        }
		   
    }
	
	
	public function user_preferences_post()
    {
       $data_val = array(
		'eid' => $this->input->post('id'),
		'application' => $this->input->post('application'),
		'value' => $this->input->post('value')
		);
		
		
        $add_preferences = $this->fpsapi->preferences($data_val);
        
       if ($add_preferences) {
            $data['status']  = 'success';
            $data['message'] = 'Preferences added successfully...';
            print_r(json_encode($data));
        } else {
            $data['status']  = 'failed';
            $data['message'] = 'Sorry preferences not insert/update...';
            print_r(json_encode($data));
        } 
    }

		
	
}