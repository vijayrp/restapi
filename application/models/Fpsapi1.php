<?php

class Fpsapi extends CI_Model {

      
	function __construct() { 
         parent::__construct(); 
      } 
	  
	 /* Get the active "message of the day"  message from  table  */
	 
    public function get_message() {
		$query = $this->db->query("SELECT * FROM `message` where now() between date and enddate");
        return $query->result();
    }
	
	
	public function add_messageoftheday($data) {
		$data['created'] = date('Y-m-d H:i:s');

        $data['status'] = 1;

		if ($this->db->insert('message', $data)) {
			$insert_id = $this->db->insert_id();
		}

		return NULL;
    }
	
	public function update_messageoftheday($data) {
		
		$data['created'] = date('Y-m-d H:i:s');
		
	    $this->db->where('id', $data['id']);
		
		$this->db->update('message', $data);
			
		//echo $this->db->last_query();

		return NULL;
    }
	
	public function delete_messageoftheday($data) {
		
		$this->db->delete('message', array('id' => $data['mid'])); 
echo $this->db->last_query();
		return NULL;
    }
	
	 /* Get the Agents from employee table  */
	public function getagents() {

         $this->db->select('employee_name as agents');
		 $this->db->distinct();
         $this->db->from('employee');
		 $this->db->where('termination_date', NULL);
		 $this->db->like('job_title', 'Advisor');
         $query = $this->db->get();
    //echo $this->db->last_query();
        return $result = $query->result();

    }
	
	public function get_escalation() {

		$this->db->from('escalations');
        $this->db->where('status','active');
        $query = $this->db->get();

        return $query->result();
	}
	
	
	
   /*  sprint post call retention dispo */  
	
	public function getDispolist() {
        $this->db->select('dispo_id,dispo_name,active');
		$this->db->from('sprint_post_call_retention_dispo');
        $query = $this->db->get();

        return $query->result();

    }
	
	public function get_post_call_retention($data) {
      
		$this->db->from('sprint_post_call_retention');
		$this->db->where($data);
        $query = $this->db->get();

        return $query->result();

    }
	
	public function update_post_call_retention($data) {
		
		$query_str='SELECT * FROM sprint_post_call_retention where 
		call_datetime = "'.$data['call_datetime'].'" and ban = "'.$data['ban'].'" and eid = "'.$data['eid'].'"';
		$query = $this->db->query($query_str);

		 if($query->num_rows()){
			// work process
		$this->db->where('call_datetime', $data['call_datetime']);
		$this->db->where('eid', $data['eid']);
		$this->db->where('ban', $data['ban']);
        $this->db->update('sprint_post_call_retention', $data);
		echo $this->db->last_query();
		$this->db->trans_complete();

		if ($this->db->affected_rows() > 0)
			{
			  return TRUE;
			}
			else
			{
			  return FALSE;
			}
						
         } /* Loop end */  
    }
	
	
	public function get_managers() {

		$this->db->select('employee_name as managers');
		$this->db->distinct();
		$this->db->from('employee');
        $this->db->where('termination_date', NULL);
		$this->db->like('job_title', 'Manager');
        $query = $this->db->get();

        return $query->result();
	}
	
	public function get_ManagerStats($data) {
      if($data['role_id']== 2){
		$sql = "select 
fb.nba_launch_rate, fb.eid, fb.nba_usage_rate, fb.twenty_plus_call_days,
fb.nba_compliance_rate, fb.pic0_rate, fb.deact_rate,fb.pic7_rate, 
fb.pic14_rate, fb.pic30_rate, emp.employee_name 
 from fact_agent_scorecard_sprint_b fb inner join employee emp 
 on emp.eid =fb.eid 
 where fb.supervisor_eid= ? and fb.month = ? group by fb.eid"; 
   $query = $this->db->query($sql, array($data['eid'], $data['month']));
   //echo $this->db->last_query();
	  }  
	  if($data['role_id']== 4){
		  
		$sql = "select 
fb.nba_launch_rate, fb.eid, fb.nba_usage_rate, fb.twenty_plus_call_days,
fb.nba_compliance_rate, fb.pic0_rate, fb.deact_rate,fb.pic7_rate, 
fb.pic14_rate, fb.pic30_rate,fb.nba_compliance_rate, emp.employee_name, 
(select employee_name from employee where eid = fb.supervisor_eid group by supervisor_eid) as supervisor_name
 from fact_agent_scorecard_sprint_b fb 
 inner join employee emp 
 on emp.eid =fb.eid  where fb.month = ? group by fb.eid";   
   $query =  $this->db->query($sql, $data['eid']);
	//echo $this->db->last_query();
	
	  }
 
        return $query->result();

    }
	
	public function get_areachart($data) {
		
	$a_procedure = "CALL get_agent_spark_sprint (?,?)";
	
    $query = $this->db->query( $a_procedure, array($data['eid'],$data['day_month']) );

        return $query->result();
	}
	
	public function get_gauges($data) {
		
		
	$a_procedure = "CALL get_agent_kpis_sprint (?,?,?)";
	
    $query = $this->db->query( $a_procedure, array($data['eid'],$data['start_date'],$data['end_date']) );

        return $query->result();
	}
	
		
}
