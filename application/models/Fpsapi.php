<?php

class Fpsapi extends CI_Model {

      
	function __construct() { 
         parent::__construct(); 
      } 
	  
	 /* Get the active "message of the day"  message from  table  */
	 
	public function getemployee($mid) {
		$query = $this->db->query("SELECT * FROM `employee` where eid='".$mid['eid']."'");
        return $query->row();
    }
	 
	 public function preferences_list() {
		$query = $this->db->query("SELECT * FROM `user_preferences` order by eid asc");
        return $query->result();
    }
	
	public function get_preferences($data) {
		$query = $this->db->query("SELECT * FROM `user_preferences` where eid='".$data['id']."'");
        return $query->result();
    }
	
	public function preferences($data) {
	    $count_sql = 'select *  from user_preferences where eid ="'.$data['eid'].'"';
        $count_query_result=$this->db->query($count_sql);

		
	if($count_query_result->num_rows()){
		
		$this->db->where('eid', $data['eid']);
        $this->db->update('user_preferences', $data);
		
		}else{
			
		$this->db->insert('user_preferences', $data);
		
		}  
		return ($this->db->affected_rows() != 1) ? false : true;
	
	
		}
	
	 public function get_messagelist() {
		$query = $this->db->query("SELECT * FROM `message` order by created desc");
        return $query->result();
    }
	 
	 
    public function get_message() {
		$query = $this->db->query("SELECT * FROM `message` where convert_tz(now(),'UTC','America/Chicago') between date and enddate");
        return $query->result();
    }
	
	 public function get_messagebyid($mid) {
		$query = $this->db->query("SELECT * FROM `message` where id='".$mid['id']."'");
		
		
        return $query->result();
    }
	
	
	
	public function add_messageoftheday($data) {

		$data['created'] = date('Y-m-d H:i:s');

        $data['status'] = 1;

		if ($this->db->insert('message', $data)) {
			$insert_id = $this->db->insert_id();
		}

		return NULL;
    }
	
	public function update_messageoftheday($data) {
		
		$data['created'] = date('Y-m-d H:i:s');
		
	    $this->db->where('id', $data['id']);
		
		$this->db->update('message', $data);
			
		//echo $this->db->last_query();

		return NULL;
    }
	
	public function delete_messageoftheday($data) {
		
		$this->db->delete('message', array('id' => $data['mid'])); 

		return NULL;
    }
	
	 /* Get the Agents from employee table  */
	public function getagents() {

         $this->db->select('employee_name as agents,email');
		 $this->db->distinct();
         $this->db->from('employee');
		 $this->db->where('termination_date', NULL);
		 $this->db->like('job_title', 'Advisor');
         $query = $this->db->get();
    //echo $this->db->last_query();
        return $result = $query->result();

    }
	
	
	public function get_agent($data) {

      $query = $this->db->query("select * from employee where termination_date is null and job_title like '%Advisor%' and eid='".$data['id']."' and CURDATE() between from_date and to_date");

      return $query->result();

    }
	
	public function get_escalation() {

		$this->db->from('escalations');
        $this->db->where('status','active');
        $query = $this->db->get();

        return $query->result();
	}
	
	
	
   /*  sprint post call retention dispo */  
	
	public function getDispolist() {
        $this->db->select('dispo_id,dispo_name,active');
		$this->db->from('sprint_post_call_retention_dispo');
        $query = $this->db->get();
        return $query->result();
    }
	
	
	public function get_post_call_retention_list($data) {
		$this->db->from('sprint_post_call_retention');
		$this->db->where('assigned_eid', $data['assigned_eid']);
        $query = $this->db->get();
		//echo $this->db->last_query();
        return $query->result();
    }
	
	
	
	public function get_post_call_retention($data) {
      
		$this->db->from('sprint_post_call_retention');
		$this->db->where($data);
        $query = $this->db->get();

        return $query->result();

    }
	
	public function update_PostCallRetention($data) {
		
		$query_str='SELECT * FROM sprint_post_call_retention where 
		call_datetime = "'.$data['call_datetime'].'" and ban = "'.$data['ban'].'" and eid = "'.$data['eid'].'"';
		$query = $this->db->query($query_str);

		 if($query->num_rows()){
			// work process
		$this->db->where('call_datetime', $data['call_datetime']);
		$this->db->where('eid', $data['eid']);
		$this->db->where('ban', $data['ban']);
        $this->db->update('sprint_post_call_retention', $data);
		$this->db->trans_complete();

		if ($this->db->affected_rows() > 0)
			{
			  return TRUE;
			}
			else
			{
			  return FALSE;
			}
						
         } /* Loop end */  
    }
	
	
	
	public function get_managers() {
		

		 $query = $this->db->query("select employee_name as managers   from employee where termination_date is null and job_title like '%Manager%'  and CURDATE() between from_date and to_date");

        return $query->result();
	}
	
	
	
	public function managerslist() {
		

		 $query = $this->db->query("select * from employee where termination_date is null and job_title like '%Manager%'  and CURDATE() between from_date and to_date");

        return $query->result();
	}
	
	
	
	public function get_manager($data) {
		
		 $query = $this->db->query("select * from employee where termination_date is null and job_title like '%Manager%' and eid='".$data['id']."' and CURDATE() between from_date and to_date");

        return $query->result();

	}
	
	public function get_ManagerStats($data) {
     
	$a_procedure ="CALL get_scorecard_sprint(?,?,?)";
	
	$query = $this->db->query( $a_procedure, array($data['eid'],$data['month'],$data['txtmonth']));
   
    return $query->result();

    }

	public function SupervisorStats($data) {

	$a_procedure ="CALL get_scorecard_sprint(?,?,?)";
	
	$query = $this->db->query( $a_procedure, array($data['eid'],$data['month'],$data['txtmonth']));
   
    return $query->result();

    }
	
	
	public function get_areachart($data) {
		
	$a_procedure = "CALL get_agent_spark_sprint (?,?)";
	
    $query = $this->db->query( $a_procedure, array($data['eid'],$data['day_month']) );

        return $query->result();
	}
	
	public function get_gauges($data) {
		
	$a_procedure = "CALL get_agent_kpis_sprint (?,?,?)";
	
    $query = $this->db->query( $a_procedure, array($data['eid'],$data['start_date'],$data['end_date']) );

        return $query->result();
	}
	
	public function get_agent_scorecard_sprint($data) {
	$a_procedure = "CALL get_agent_scorecard_sprint(?,?)";
    $query = $this->db->query( $a_procedure, array($data['month'],$data['eid']) );
        return $query->result();
	}
	
	public function get_metric_tiers_sprint($data) {
		
	$a_procedure = "CALL get_metric_tiers_sprint(?,?)";
    $query = $this->db->query( $a_procedure, array($data['eid'],$data['scorecard_lvl']) );
        return $query->result();
	}
		
	public function get_agent_scorecard_spark_sprint($data) {
	$a_procedure = "CALL get_agent_scorecard_spark_sprint(?)";
    $query = $this->db->query($a_procedure, array($data['eid']));
    //echo $this->db->last_query();
        return $query->result();
	}
	
	public function get_agent_scorecard_sprint_new($data) {
		$a_procedure = "CALL get_scorecard_sprint(?,?,'')";
    $query = $this->db->query( $a_procedure, array($data['eid'],$data['month']) );
	 //echo $this->db->last_query();
        return $query->result();
	}
	
	public function postescalation($data) {
		
		$calldatetime = $data['calldatetime'];
		$agentname = $data['agentname'];
		$customername = $data['customername'];
		$bannumber = $data['bannumber'];
		$marketvalue = $data['marketvalue'];
		$nolines = $data['nolines'];
		$nbaoffers = $data['nbaoffers'];
		$nbareason = $data['nbareason'];
		$description = $data['description'];
		$stepstaken = $data['stepstaken'];
		$customerdesire = $data['customerdesire'];
		$manager = $data['manager'];
		$callbackdatetime = $data['callbackdatetime'];
		$status = $data['status'];
		
		
		$count_sql = 'select *  from escalations where bannumber ="'.$bannumber.'"';
        $count_query_result=$this->db->query($count_sql);

		
	if($count_query_result->num_rows()){
		
		$this->db->where('bannumber', $bannumber);
        $this->db->update('escalations', $data);
		
		}else{
			
		$this->db->insert('escalations', $data);
		
		}  
		return ($this->db->affected_rows() != 1) ? false : true;
    }
	
		
}
